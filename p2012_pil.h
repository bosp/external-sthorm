// ====================================================================
//                         STMicroelectronics
//    Reproduction and Communication of this document is strictly
//    prohibited unless specifically authorized in writing by
//    STMicroelectronics.
// ====================================================================
//
// <B>
// \n
// This file is part of the Platform 2012 program,
// a cooperation between STMicroelectronics and CEA.\n
// Redistribution of this file to outside parties is
// strictly prohibited without the written consent
// of the module owner indicated below.\n
// </B>
//
// \par  Module owner:
//       Germain Haugou, STMicroelectronics (germain.haugou@st.com)
//
// \par  Copyright (C) 2009 STMicroelectronics
//
// \par  Authors:
//       Germain Haugou (germain.haugou@st.com)
//       Patrick Bellasi (derkling@gmail.com)
//       Giuseppe Massari (joe.massanga@gmail.com)
//
// ====================================================================

#ifndef _P2012_PIL_
#define _P2012_PIL_


#include <stdint.h>
#include <p2012_rt_conf.h>

/**
 * General description of the managed device.
 */
typedef struct DeviceDescriptor {
	uint16_t idVendor;
	uint16_t idProduct;

#define DEVICE_NAME_MAX 8
	char name[DEVICE_NAME_MAX];

#define DEVICE_FW_VERSION_MAX 12
	char fw_ver[DEVICE_FW_VERSION_MAX];

	// Describes each programming model that is supported
#define DEVICE_CAPABILITIES_NPM     0x01
#define DEVICE_CAPABILITIES_OPENCL  0x02
	uint8_t caps;

	// For each programming model, describes which capability is supported
#define DEVICE_RT_CAPABILITIES_RTM_BW    0x01	// Bandwidth allocation
#define DEVICE_RT_CAPABILITIES_RTM_PB    0x02	// Power dudget
	uint8_t rtCaps[2];

} DeviceDescriptor_t;


/**
 * Configuration of the notification interface.
 *
 * Thes should define the set of events which are of interest for the
 * System-Wide Run-Time Resource Manager (SW-RTRM). Each of these events
 * should genereta a message from the device firmware to the SW-RTRM.
 */
typedef struct NotifyDescriptor {

	// TODO to be completed...
#define NOTIFY_EVENT_AVAILABILITY   0x0001
#define NOTIFY_EVENT_FREQUENCY      0x0002
#define NOTIFY_EVENT_THERMAL        0x0004
	uint32_t events_mask;

	// TODO add definition for events "rates" and "thresholds"

	uint32_t hostQueueId;
	uint32_t hostQueueFabricAddr;

} NotifyDescriptor_t;

// TODO add definition of notification messages

typedef struct ClusterPowerModel {

	struct {
		uint32_t hz;
		uint32_t watts;
	} min;

	struct {
		uint32_t hz;
		uint32_t watts;
	} max;

	struct {
		uint32_t hz;
		uint32_t watts;
	} cur;

} ClusterPowerModel_t;

typedef struct PePowerModel {

	struct {
		uint32_t watts;
	} min;

	struct {
		uint32_t watts;
	} max;

	struct {
		uint32_t watts;
	} cur;

} PePowerModel_t;


/**
 * The descriptor of a single PE
 */
typedef struct PeDescriptor {
	// Currently running EXC, -1 if no EXC is running
	int32_t exc_id;

	// PE level Power Model
	PePowerModel_t power;

} PeDescriptor_t;


/**
 * The descriptor of a single DMA channel
 */
typedef struct DmaDescriptor {
	struct {
		uint32_t max;
		uint32_t cur;
	} bandwidth;
} DmaDescriptor_t;


/**
 * The descriptor of a single cluster
 */
typedef struct ClusterDescriptor {
	uint8_t id;

#define CLUSTER_FEATURES_RUNTIME_NPM    0x01
#define CLUSTER_FEATURES_RUNTIME_OPENCL 0x02
	uint8_t features;

#define CLUSTER_THERMAL_SENSORS_MAX 5
	uint8_t temp_count;
	uint32_t temp[CLUSTER_THERMAL_SENSORS_MAX];
	struct {
		uint32_t warn;
		uint32_t crit;
	} temp_threshold;

	// Cluster shared (data) memory [KB]
	uint32_t dmem_kb;

#define CLUSTER_PES_MAX 16
	PeDescriptor_t pe[CLUSTER_PES_MAX];

#define CLUSTER_DMAS_MAX 2
	DmaDescriptor_t dma[CLUSTER_DMAS_MAX];

	// Cluster level Power Model
	ClusterPowerModel_t power;

} ClusterDescriptor_t;


/**
 * Should provide a complete and run-time updated view of the kind number and
 * status of the available computational resources.
 */
typedef struct PlatformDescriptor {


#define PLATFORM_CLUSTERS_MAX 4
	uint8_t clusters_count;
	ClusterDescriptor_t cluster[PLATFORM_CLUSTERS_MAX];

} PlatformDescriptor_t;

/**
 * Constraints of a single EXC.
 */
typedef struct ExcConstraints {
	// A unique identifier of the application
	uint32_t id;
#define EXC_NAME_MAX 16
	char name[EXC_NAME_MAX];

	union {

		struct {
			// A bitfield of clusters where this EXC should be deployed
			uint32_t cluster_map;
			// The % of time an application could use the assigned cluster
			uint16_t cluster_quota[PLATFORM_CLUSTERS_MAX];
			// Expected performances
			uint32_t cluster_hz[PLATFORM_CLUSTERS_MAX];

			// Energy budget
			struct {
				// Energy budget in [mW]
				uint16_t budget_mw;
				// Period in [ms] of budget allocation
				uint16_t period_ms;
			} energy;

			// Memory budget
			struct {
				// Allocatd Bytes [B] into L1 data memory
				uint32_t L1_B;
				// Allocated MBytes [KB] into L2 data memory
				uint32_t L2_KB;
			} dmem;
		} generic;

		struct {
			// The average % of resources an application could use the fabric.
			uint16_t fabric_quota;

			// Energy budget
			struct {
				// Energy budget in [mW]
				uint16_t budget_mw;
			} energy;

		} ocl;

		struct {
		} npm;
	} u;

} ExcConstraints_t;

/**
 * The set of currently active constraints.
 * This collect one constraint for each and single run-time managed exeuction
 * context
 */
typedef struct PlatformConstraints {

	uint8_t count;

	// Maximum number of different EXecution Contexts (EXCs)
#define EXCS_MAX PLATFORM_CLUSTERS_MAX*16
	ExcConstraints_t exc[EXCS_MAX];


} PlatformConstraints_t;


/**
 * The main device descriptor.
 * This collects all the information related to platform description and
 * run-time management.
 */
typedef struct ManagedDevice {

	// This item is for compatibility
	runtimeConf_t rtConf;

	// Global Configuration
	DeviceDescriptor_t descr;

	// Notificaiton Interface
	NotifyDescriptor_t notif;

	// Platform Description Interface
	PlatformDescriptor_t pdesc;

	// Platform Constraints
	PlatformConstraints_t pcons;

} ManagedDevice_t;


#endif
